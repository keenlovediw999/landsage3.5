import firebase from 'firebase/compat/app';
import 'firebase/auth';

const firebaseConfig = firebase.initializeApp({
    apiKey: "AIzaSyCB4jda3-f20zGwaDF1aigENkK0XLKMgXo",
    authDomain: "react-auth-b9a8b.firebaseapp.com",
    projectId: "react-auth-b9a8b",
    storageBucket: "react-auth-b9a8b.appspot.com",
    messagingSenderId: "985262223241",
    appId: "1:985262223241:web:e85193929d5f6c27297b58",
    measurementId: "G-SL7XCQ37Y7"
  });

  export default firebaseConfig;