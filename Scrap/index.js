const puppeteer = require("puppeteer");
const fs = require("fs/promises");
const cron  = require('node-cron');
let name = 1;
a = "Succeseful"
async function start() {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto("https://api.mrcmekong.org/api/v1/nrtm/station")
  let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);
// current month
let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
// current year
let year = date_ob.getFullYear();
// current hours
let hours = date_ob.getHours();
// current minutes
let minutes = date_ob.getMinutes();
// current seconds
let seconds = date_ob.getSeconds();
  const names = await page.evaluate(() => {
    return Array.from(document.querySelectorAll('body')).map(x => x.innerText)
  })
  await fs.writeFile( year + "-" + month + "-" + date + "_" + hours + "-" + minutes + '_Mrc_data.json', names)
  console.log(a+'_'+date+'-'+month+'-'+year+ "_" + hours + "-" + minutes)
  await browser.close()
  name ++;
}

cron.schedule("0,15,30,45 * * * *", start)
