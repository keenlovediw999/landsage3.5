import React, { Component, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import Addpinform from "./formComponent/addPinForm";
import Updatepinform from "./formComponent/updatePinForm";
import AddPinfileform from "./formComponent/addPinfileform";
import EditPindataform from "./formComponent/editPinDataform";
import AddTextureForm from "./formComponent/addTextureForm";
import UpdateTextureForm from "./formComponent/updateTextureForm";
import EditTexturedataform from "./formComponent/editTextureDataform";
import AddTexturefileform from "./formComponent/addTexturefileform";
import AddSubTypeForm from "./formComponent/addSubTypeForm";
//modal component for rendering modal and modal
function Modalwindow(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const renderInputField = () => {
    switch (props.action) {
      case "addStation":
        return (
          <React.Fragment>
            <Button
              style={{ marginBottom: "10px" }}
              variant="primary"
              onClick={handleShow}
            >
              Add Station
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Add Station Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Addpinform
                  handleAdd={props.handleAdd}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "addTexture":
        return (
          <React.Fragment>
            <Button
              style={{ marginBottom: "10px" }}
              variant="primary"
              onClick={handleShow}
            >
              Add Layer
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Add Layer Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <AddTextureForm
                  handleAdd={props.handleAdd}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "editStation":
        return (
          <React.Fragment>
            <Button variant="btn btn-success btn-sm" onClick={handleShow}>
              Edit
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Edit Station Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Updatepinform
                  originalState={props.originalState}
                  originalName={props.originalName}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "editTexture":
        return (
          <React.Fragment>
            <Button variant="btn btn-success btn-sm" onClick={handleShow}>
              Edit
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Edit Layer Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <UpdateTextureForm
                  originalState={props.originalState}
                  originalId={props.originalId}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "addPinData":
        return (
          <React.Fragment>
            <Button variant="btn btn-primary btn-sm mr-3" onClick={handleShow}>
              Add Data
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Add Data to Station</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <AddPinfileform
                  originalState={props.originalState}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "addTextureData":
        return (
          <React.Fragment>
            <Button variant="btn btn-primary btn-sm mr-3" onClick={handleShow}>
              Add File
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Add File to Layer</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <AddTexturefileform
                  originalState={props.originalState}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "editPinData":
        return (
          <React.Fragment>
            <Button variant="btn btn-primary btn-sm mr-3" onClick={handleShow}>
              Edit Data
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Edit Staion Data </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <EditPindataform
                  originalState={props.originalState}
                  originalFile={props.originalFile}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "editTextureData":
        return (
          <React.Fragment>
            <Button variant="btn btn-primary btn-sm mr-3" onClick={handleShow}>
              Edit Data
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Edit Layer Data </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <EditTexturedataform
                  originalState={props.originalState}
                  originalFile={props.originalFile}
                  handleUpdate={props.handleUpdate}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
      case "addsubType":
        return (
          <React.Fragment>
            <Button
              style={{ marginBottom: "10px" }}
              variant="primary"
              onClick={handleShow}
            >
              Add Subtype
            </Button>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>Add Subtype</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <AddSubTypeForm
                  Wheel={props.Wheel}
                  handleAdd={props.handleAdd}
                  handleClose={handleClose}
                />
              </Modal.Body>
            </Modal>
          </React.Fragment>
        );
    }
  };
  return (
    <div>
      {/* conditional button rendering for add and update station modal */}
      {renderInputField()}
    </div>
  );
}

export default Modalwindow;
