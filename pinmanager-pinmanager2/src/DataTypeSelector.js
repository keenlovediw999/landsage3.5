const mainType = [
  { _id: "Flooding", name: "Flooding" },
  { _id: "Landslide", name: "Landslide" },
];

const subType = {
  flooding: [
    { _id: "Discharge", name: "Discharge" },
    { _id: "Rainfall", name: "Rainfall" },
    { _id: "WaterLevel", name: "WaterLevel" },
  ],
  landslide: [
    { _id: "Road", name: "Road" },
    { _id: "Population", name: "Population" },
    { _id: "Housing", name: "Housing" },
    { _id: "Landuse", name: "Landuse" },
    { _id: "Geomorphology", name: "Geomorphology" },
    { _id: "Geology", name: "Geology" },
    { _id: "DEM", name: "DEM" },
    { _id: "Slope", name: "Slope" },
  ],
};
const visualFormat = {
  timeSeries: [{ _id: "TimeSeries", name: "TimeSeries" }],
  textureMap: [{ _id: "TextureMap", name: "TextureMap" }],
};

export { mainType, subType, visualFormat };
