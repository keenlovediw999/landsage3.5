import React, { Component } from "react";
import http from "./services/httpService";
import config from "./config.js";
import Modalwindow from "./modal";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
class RenderWheelManager extends Component {
  state = { posts: "" };
  async componentDidMount() {
    const { data: posts } = await http.get(config.wheelEndpoint);
    this.setState({ posts });
  }

  handleAdd = async (obj, main, sub) => {
    const { data: post } = await http.put(
      config.wheelEndpoint + `/${main}/${sub}`
    );
    this.setState({ posts: obj });
  };
  handleDelete = async (maintype, subtype) => {
    const { data: post } = await http.delete(
      config.wheelEndpoint + `/${maintype}/${subtype}`
    );

    const remain_sub = this.state.posts.subType[maintype].filter(
      (sub) => sub.name !== subtype
    );
    const posts = { ...this.state.posts };
    posts.subType[maintype] = remain_sub;
    //console.log(posts);
    this.setState({ posts });
  };
  render() {
    const { posts } = this.state;
    return (
      <div className="container" style={{ marginTop: 20 }}>
        <h1>Subtype list</h1>
        <Modalwindow
          action="addsubType"
          Wheel={posts}
          handleAdd={this.handleAdd}
        />
        <div className="card">
          <div className="card-header">Flooding</div>
          <ul className="list-group list-group-flush">
            {posts !== ""
              ? this.state.posts.subType.Flooding.map((post) => (
                  <li className="list-group-item" key={post.name}>
                    <div>
                      {post.name}
                      <i
                        className="fa fa-minus-circle red"
                        onClick={() =>
                          confirmAlert({
                            title: `Delete confirmation`,
                            message: `Are you cure to delete \"${post.name}\"?`,
                            buttons: [
                              {
                                label: "Yes",
                                onClick: () => {
                                  this.handleDelete("Flooding", post.name);
                                },
                              },
                              {
                                label: "No",
                                onClick: () => {},
                              },
                            ],
                            "&.react-confirm-alert-overlay": {
                              zIndex: 99999,
                            },
                          })
                        }
                        style={{ cursor: "pointer", color: "red" }}
                        aria-hidden="true"
                      ></i>
                    </div>
                  </li>
                ))
              : null}
          </ul>
        </div>
        <div className="card" style={{ marginTop: 20 }}>
          <div className="card-header">Landslide</div>
          <ul className="list-group list-group-flush">
            {posts !== ""
              ? this.state.posts.subType.Landslide.map((post) => (
                  <li className="list-group-item" key={post.name}>
                    <div>
                      {post.name}
                      <i
                        className="fa fa-minus-circle red"
                        onClick={() =>
                          confirmAlert({
                            title: `Delete confirmation`,
                            message: `Are you cure to delete \"${post.name}\"?`,
                            buttons: [
                              {
                                label: "Yes",
                                onClick: () => {
                                  this.handleDelete("Landslide", post.name);
                                },
                              },
                              {
                                label: "No",
                                onClick: () => {},
                              },
                            ],
                            "&.react-confirm-alert-overlay": {
                              zIndex: 99999,
                            },
                          })
                        }
                        style={{ cursor: "pointer", color: "red" }}
                        aria-hidden="true"
                      ></i>
                    </div>
                  </li>
                ))
              : null}
          </ul>
        </div>
      </div>
    );
  }
}

export default RenderWheelManager;
