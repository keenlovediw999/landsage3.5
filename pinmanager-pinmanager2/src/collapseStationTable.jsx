import React, { Component } from "react";
import Badges from "./common/badge";
import BootstrapTable from "react-bootstrap-table-next";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import Collapse from "@material-ui/core/Collapse";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Box from "@material-ui/core/Box";
import { Tab } from "bootstrap";
import Typography from "@material-ui/core/Typography";
import { render } from "react-dom";
import Modalwindow from "./modal";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
const StyledCell = withStyles({
  head: {
    fontWeight: 700,
    fontSize: 20,
  },
  root: {
    backgroundColor: "white",
  },
})(TableCell);
class CollapseStationtable extends Component {
  state = { open: false };
  handleOpen = () => {
    const open = !this.state.open;
    this.setState({ open });
  };
  render() {
    const { post } = this.props;
    return (
      <React.Fragment>
        <TableRow>
          <StyledCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => this.handleOpen()}
            >
              {this.state.open ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )}
            </IconButton>
          </StyledCell>
          <StyledCell component="th" scope="row">
            {post.name}
          </StyledCell>
          <StyledCell align="middle">{post.country}</StyledCell>
          <StyledCell align="middle">{post.lat}</StyledCell>
          <StyledCell align="middle">{post.long}</StyledCell>
          <StyledCell align="middle">
            {post.fileList.map((file) => (
              <Badges
                key={file.subType}
                mainType={file.mainType}
                subType={file.subType}
              />
            ))}
          </StyledCell>
          <StyledCell align="middle">
            <div className="btn-group">
              <Modalwindow
                action="addPinData"
                originalState={post}
                handleUpdate={this.props.handleUpdate}
              />

              <Modalwindow
                action="editStation"
                originalState={post}
                handleUpdate={this.props.handleUpdate}
              />

              <button
                className="btn btn-danger btn-sm ml-3"
                onClick={() =>
                  confirmAlert({
                    title: `Delete confirmation`,
                    message: `Are you cure to delete \"${post.name}\"?`,
                    buttons: [
                      {
                        label: "Yes",
                        onClick: () => {
                          this.props.handleDelete(post);
                        },
                      },
                      {
                        label: "No",
                        onClick: () => {},
                      },
                    ],
                    "&.react-confirm-alert-overlay": {
                      zIndex: 99999,
                    },
                  })
                }
              >
                Delete
              </button>
            </div>
          </StyledCell>
        </TableRow>
        <TableRow>
          <StyledCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={this.state.open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                  Data List
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <StyledCell>mainType</StyledCell>
                      <StyledCell>subType</StyledCell>
                      <StyledCell>Visualize Format</StyledCell>
                      <StyledCell>FileName</StyledCell>
                      <StyledCell>Custom1</StyledCell>
                      <StyledCell>Custom2</StyledCell>
                      <StyledCell>Custom3</StyledCell>
                      <StyledCell>Custom4</StyledCell>
                      <StyledCell>Custom5</StyledCell>
                      <StyledCell>Action</StyledCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {post.fileList.map((file) => (
                      <TableRow key={file.subType}>
                        <StyledCell component="th" scope="row">
                          {file.mainType}
                        </StyledCell>
                        <StyledCell>{file.subType}</StyledCell>
                        <StyledCell>{file.visualFormat}</StyledCell>
                        <StyledCell align="middle">
                          <a
                            href={`http://localhost:5000/csv/${file.fileName}`}
                          >
                            {file.fileName}
                          </a>
                        </StyledCell>
                        {file.custom.map((custom) =>
                          custom.key !== "" && custom.value !== "" ? (
                            <StyledCell align="middle">
                              {custom.key}: {custom.value}
                            </StyledCell>
                          ) : (
                            <StyledCell>-</StyledCell>
                          )
                        )}

                        <StyledCell align="middle">
                          <div className="btn-group">
                            <Modalwindow
                              action="editPinData"
                              originalFile={file}
                              originalState={post}
                              handleUpdate={this.props.handleUpdate}
                            />

                            <button
                              className="btn btn-danger btn-sm"
                              onClick={() =>
                                confirmAlert({
                                  title: `Delete confirmation`,
                                  message: `Are you cure to delete \"${file.fileName}\"?`,
                                  buttons: [
                                    {
                                      label: "Yes",
                                      onClick: () => {
                                        this.props.handleDelData(post, file);
                                      },
                                    },
                                    {
                                      label: "No",
                                      onClick: () => {},
                                    },
                                  ],
                                  "&.react-confirm-alert-overlay": {
                                    zIndex: 99999,
                                  },
                                })
                              }
                            >
                              Delete
                            </button>
                          </div>
                        </StyledCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </StyledCell>
        </TableRow>
      </React.Fragment>
    );
  }
}
export default CollapseStationtable;
