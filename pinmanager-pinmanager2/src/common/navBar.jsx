import React, { Component } from "react";
import { NavLink } from "react-router-dom";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light ">
        <div className="container-fluid">
          <a className="navbar-brand">LandSage3 Data Manager</a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <NavLink
              to="/stationmanager"
              className="nav-item nav-link"
              aria-current="page"
            >
              Monitoring Station
            </NavLink>
            <NavLink to="/texturemanager" className="nav-item nav-link">
              Image Layer
            </NavLink>
            <NavLink to="/filemanager" className="nav-item nav-link">
              File Manager
            </NavLink>
            <NavLink to="/wheelmanager" className="nav-item nav-link">
              Add Subtype
            </NavLink>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
