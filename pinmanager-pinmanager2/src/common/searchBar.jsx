import React, { Component } from "react";

const SearchBar = ({ value, onChange, place }) => {
  return (
    <div>
      <input
        type="Text"
        name="query"
        value={value}
        className="form-control my-3"
        placeholder={place}
        onChange={(e) => onChange(e.currentTarget.value)}
      />
    </div>
  );
};

export default SearchBar;
