import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import RenderStation from "./renderStation";
import NotFound from "./common/notFound";
import NavBar from "./common/navBar";
import RenderTextureManager from "./renderTextureManager";
import RenderFileManager from "./renderFileManager";
import RenderWheelManager from "./renderWheelManager";

class App extends Component {
  render() {
    //console.log(process.env.REACT_APP_URL);
    return (
      <main>
        <NavBar />
        <Switch>
          <Route path="/texturemanager" component={RenderTextureManager} />
          <Route path="/filemanager" component={RenderFileManager} />
          <Route path="/wheelmanager" component={RenderWheelManager} />
          <Route path="/notfound" component={NotFound} />
          <Route path="/stationmanager" component={RenderStation} />
          <Redirect from="/" exact to="stationmanager" />
          <Redirect to="/notfound" />
        </Switch>
      </main>
    );
  }
}

export default App;
