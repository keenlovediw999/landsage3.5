import React, { Component } from "react";
import { ToastContainer } from "react-toastify";
import http from "./services/httpService";
import config from "./config.js";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Modalwindow from "./modal";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import { Tab } from "bootstrap";
import { withStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { render } from "react-dom";
import TableCell from "@material-ui/core/TableCell";
import CollapseTexturetable from "./collapseTextureTable";
import _ from "lodash";
import SearchBar from "./common/searchBar";

const StyledCell = withStyles({
  head: {
    fontWeight: 700,
    fontSize: 20,
  },
  root: {
    backgroundColor: "white",
  },
})(TableCell);

class RenderTextureManager extends Component {
  state = {
    open: false,
    posts: [],
    modalState: false,
    searchQuery: "",
  };
  async componentDidMount() {
    const { data: posts } = await http.get(config.textureEndpoint);
    this.setState({ posts });
  }
  //functon for open and close modal
  handleModalState = (modalState) => {
    modalState = !modalState;
    this.setState({ modalState });
  };
  handleAdd = async (obj) => {
    const { data: post } = await http.post(config.textureEndpoint, obj);
    //console.log(post);
    const posts = [post, ...this.state.posts];
    this.setState({ posts });
  };
  //fucntion for put payload(edit existing  station) to backend
  handleUpdate = async (post) => {
    //console.log(post);
    const result = await http.put(
      config.textureEndpoint + "/" + post._id,
      post
    );
    //console.log(result);
    const posts = [...this.state.posts];
    const index = posts.map((post) => post._id).indexOf(post._id);
    posts[index] = { ...post };
    //console.log(posts);
    this.setState({ posts });
  };
  handleDelete = async (post) => {
    const originalPosts = this.state.posts;

    const posts = this.state.posts.filter((p) => p._id !== post._id);
    this.setState({ posts });
    try {
      await http.delete(config.textureEndpoint + "/" + post._id);
    } catch (ex) {
      //expected(404 not found, 400 bad request)
      // display a specific error message
      //console.log(ex.response);
      if (ex.response && ex.response.status === 404) {
        alert("This post has already been deleted");
        //console.log("error:", ex);
      }

      //Unexpected (network down , server down, db down , bug)
      // Display a generic and friendly error message

      this.setState({ posts: originalPosts });
    }
  };
  handleSearch = (search) => {
    this.setState({ searchQuery: search });
  };
  handleDelData = async (row, file) => {
    const FL = [...row.fileList];
    const newFL = FL.filter(
      (payloadfile) => payloadfile.textureFilename !== file.textureFilename
    );
    //console.log(FL);
    //console.log(newFL);
    const payload = {
      _id: row._id,
      textureName: row.textureName,
      country: row.country,
      mainType: row.mainType,
      subType: row.subType,
      fileList: [...newFL],
    };
    try {
      await http.delete(config.fileEndpoint + "/" + file.textureFilename);
    } catch (ex) {
      //expected(404 not found, 400 bad request)
      // display a specific error message
      //console.log(ex.response);
      if (ex.response && ex.response.status === 404) {
        alert("Something  Went wrong");
        //console.log("error:", ex);
      }
    }
    const legendFile = file.legendFilename;
    const count_lg = row.fileList.filter(
      (file) => file.legendFilename === legendFile
    ).length;
    // condition for delete legend file (if lengend file only used by deleting file , delete it)
    if (count_lg === 1) {
      try {
        await http.delete(config.fileEndpoint + "/" + legendFile);
      } catch (ex) {
        //expected(404 not found, 400 bad request)
        // display a specific error message
        //console.log(ex.response);
        if (ex.response && ex.response.status === 404) {
          alert("Something  Went wrong");
          //console.log("error:", ex);
        }
      }
    }
    //console.log(payload);
    this.handleUpdate(payload);
  };
  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  };

  render() {
    const sorted_posts = _.orderBy(
      this.state.posts,
      ["country", "subType"],
      ["asc", "asc"]
    );
    //console.log(sorted_posts);
    var filtered_search = this.state.posts;
    const { searchQuery } = this.state;
    if (searchQuery) {
      filtered_search = sorted_posts.filter((post) =>
        post.textureName.toLowerCase().includes(searchQuery.toLowerCase())
      );
    }
    return (
      <div
        style={{
          // backgroundColor: "#F8F9FA",
          backgroundColor: "#F8F9FA",
          minHeight: "100vh",
          padding: "30px",
        }}
      >
        <ToastContainer />
        <h1>Layer List</h1>
        <Modalwindow action="addTexture" handleAdd={this.handleAdd} />
        <SearchBar
          value={searchQuery}
          onChange={this.handleSearch}
          place={"Search Layer Name ..."}
        />
        <div>
          <TableContainer
            style={{ backgroundColor: "#F8F9FA" }}
            component={Paper}
          >
            <Table
              // style={{
              //   borderCollapse: "separate",
              //   borderSpacing: "0 10px",
              // }}
              aria-label="simple table"
            >
              <TableHead>
                <TableRow>
                  <StyledCell
                    style={{ fontWeight: 700 }}
                    align="middle"
                  ></StyledCell>
                  <StyledCell align="middle">Layer Name</StyledCell>
                  <StyledCell align="middle">Country</StyledCell>
                  <StyledCell align="middle">Main Type</StyledCell>
                  <StyledCell align="middle">Sub Type</StyledCell>
                  <StyledCell align="middle">File Amount</StyledCell>
                  <StyledCell align="middle">Action</StyledCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filtered_search.map((post) => (
                  <CollapseTexturetable
                    key={post._id}
                    post={post}
                    handleUpdate={this.handleUpdate}
                    handleDelete={this.handleDelete}
                    handleDelData={this.handleDelData}
                  />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    );
  }
}

export default RenderTextureManager;
