import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import Wheel from "../WheelList.json";
import config from "../config";
import http from "../services/httpService";
import { ThumbDownSharp } from "@material-ui/icons";
class AddSubTypeForm extends Form {
  state = {
    dataHeader: {
      mainType: "",
      subType: "",
    },
    errors: {},
  };

  schema = {
    mainType: Joi.string().required().label("Main DataType"),
    subType: Joi.string().required().label("Sub Data Type"),
  };

  doSubmit = () => {
    const payload = { ...this.props.Wheel };

    payload.subType[this.state.dataHeader.mainType].push({
      _id: this.state.dataHeader.subType,
      name: this.state.dataHeader.subType,
    });
    this.props.handleAdd(
      payload,
      this.state.dataHeader.mainType,
      this.state.dataHeader.subType
    );
    this.props.handleClose();
  };
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {this.renderSelect("mainType", "Main DataType", Wheel.mainType)}
          {this.renderInput("subType", "Sub DataType")}
          <button
            style={{ margin: 10 }}
            disabled={this.validate()}
            className="btn btn-primary float-right"
          >
            Add Subtype
          </button>
        </form>
      </div>
    );
  }
}

export default AddSubTypeForm;
