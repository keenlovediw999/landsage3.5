import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import http from "../services/httpService";
import { FileUpload } from "../common/fileUpload";
//import { mainType, subType, visualFormat } from "../DataTypeSelector.js";
import Wheel from "../WheelList.json";
import Progress from "../common/progress";
import config from "../config";
// extend the rendering input and validate function from Form component
class EditTexturedataform extends Form {
  // set state according to the original data
  state = {
    _id: this.props.originalState._id,
    data: [],
    uploadPercentage: 0,
    dataHeader: {
      date: "",
      month: "",
      year: "",
      N: "",
      S: "",
      E: "",
      W: "",
      textureFilename: "",
      legendFilename: "",
      legendcustom: "Temp",
    },
    fileList: this.props.originalState.fileList,
    errors: {},
  };
  async componentDidMount() {
    const dataHeader = {
      date: this.props.originalFile.date,
      month: this.props.originalFile.month,
      year: this.props.originalFile.year,
      N: this.props.originalFile.border.N,
      S: this.props.originalFile.border.S,
      E: this.props.originalFile.border.E,
      W: this.props.originalFile.border.W,
      textureFilename: this.props.originalFile.textureFilename,
      legendFilename: this.props.originalFile.legendFilename,
    };
    this.setState({ dataHeader });
  }
  // define schema for validate input data
  schema = {
    date: Joi.number().min(1).max(31).required().label("Date"),
    month: Joi.number().min(1).max(12).required().label("Month"),
    year: Joi.number().min(2000).max(2070).required().label("Year"),
    N: Joi.number().min(-180).max(180).required().label("North"),
    E: Joi.number().min(-180).max(180).required().label("East"),
    S: Joi.number().min(-180).max(180).required().label("South"),
    W: Joi.number().min(-180).max(180).required().label("West"),
    textureFilename: Joi.string().required(),
    legendFilename: Joi.string().required(),
    legendcustom: Joi.string(),
  };

  dateValidate = () => {
    //Fix
    const excludeOrigin = this.props.originalState.fileList.filter(
      (x) => x.textureFilename !== this.props.originalFile.textureFilename
    );
    //console.log(excludeOrigin);
    const existDate = excludeOrigin.map((file) => ({
      date: file.date,
      month: file.month,
      year: file.year,
    }));

    const sort = existDate.sort(
      (a, b) => a.year - b.year || a.month - b.month || a.date - b.date
    );

    if (
      existDate.some(
        (x) =>
          x.date.toString() === this.state.dataHeader.date.toString() &&
          x.month.toString() === this.state.dataHeader.month.toString() &&
          x.year.toString() === this.state.dataHeader.year.toString()
      )
    )
      return (
        <div>
          <p style={{ color: "red" }}>*Date Already exist in the record!</p>{" "}
          <br />
          <p>DateList</p>
          {existDate.map((date) =>
            date.date.toString() === this.state.dataHeader.date.toString() &&
            date.month.toString() === this.state.dataHeader.month.toString() &&
            date.year.toString() === this.state.dataHeader.year.toString() ? (
              <p style={{ fontSize: "13px", color: "red" }}>
                [ {date.date}-{date.month}-{date.year} ]
              </p>
            ) : (
              <p style={{ fontSize: "13px" }}>
                [ {date.date}-{date.month}-{date.year} ]
              </p>
            )
          )}
        </div>
      );
    else return null;
  };
  fileValidate = () => {
    const fileComplete =
      this.state.dataHeader.legendFilename === "Custom"
        ? this.state.dataHeader.legendcustom !== "Temp"
          ? true
          : false
        : true;
    return !(
      this.validate() === null &&
      fileComplete &&
      this.dateValidate() === null
    );
  };
  // function for upload csv file
  handleUpload = async (file, type) => {
    if (type === "File") {
      const dataHeader = {
        ...this.state.dataHeader,
        fileName: file.name,
      };
      this.setState({ dataHeader });
    } else if (type === "LegendFile") {
      const dataHeader = {
        ...this.state.dataHeader,
        legendcustom: file.name,
      };
      this.setState({ dataHeader });
    }

    const formData = new FormData();
    formData.append("file", file);
    //upload data to backend
    try {
      const res = await http.post(config.dataEndpoint, formData, {
        headers: {
          "Context-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      if (err.response.status === 500) {
        //console.log("There was a problem with the server");
      }
    }
  };
  renderLegendSelect() {
    const existLegend = this.props.originalState.fileList.map(
      (file) => file.legendFilename
    );
    const uniExistLegend = [...new Set(existLegend), "Custom"];

    const legendWheel = uniExistLegend.map((legend) => ({
      _id: legend,
      name: legend,
    }));
    //console.log(legendWheel);
    // switch (this.state.dataHeader.maintype) {
    //   case "Flooding":
    return this.renderSelect("legendFilename", "Legend File", legendWheel);
    //   case "Landslide":
    //     return this.renderSelect("subtype", "Sub DataType", subType.landslide);
    // }
  }
  renderLegendUpload() {
    if (this.state.dataHeader.legendFilename === "Custom") {
      return (
        <React.Fragment>
          <FileUpload type="LegendFile" handleUpload={this.handleUpload} />
          {this.state.uploadPercentage === 0 ? null : (
            <Progress percentage={this.state.uploadPercentage} />
          )}{" "}
        </React.Fragment>
      );
    }
  }
  //function for put the data(update existing data )
  doSubmit = () => {
    //submit edit data to database
    let filetype = "texture";
    //console.log("Submitted");
    const FL = [...this.props.originalState.fileList];
    FL.map((file) =>
      file.textureFilename === this.props.originalFile.textureFilename
        ? ((file.textureFilename = this.state.dataHeader.textureFilename),
          (file.legendFilename =
            this.state.dataHeader.legendFilename === "Custom"
              ? this.state.dataHeader.legendcustom
              : this.state.dataHeader.legendFilename),
          (file.date = this.state.dataHeader.date),
          (file.month = this.state.dataHeader.month),
          (file.year = this.state.dataHeader.year),
          (file.fileType = filetype),
          (file.border = {
            N: this.state.dataHeader.N,
            S: this.state.dataHeader.S,
            E: this.state.dataHeader.E,
            W: this.state.dataHeader.W,
          }))
        : null
    );
    //console.log(FL);
    const payload = {
      textureName: this.props.originalState.textureName,
      country: this.props.originalState.country,
      mainType: this.props.originalState.mainType,
      subType: this.props.originalState.subType,
      _id: this.props.originalState._id,
      fileList: [...FL],
    };
    //console.log(payload);
    this.props.handleUpdate(payload);
    this.props.handleClose();
  };
  rendersubSelect() {
    const existSubtype = this.state.fileList.map(
      (file) => file.subType !== this.originalsubType
    );

    //console.log(existSubtype);
    const filterLandslideSubtype = [...Wheel.subType.landslide];
    // console.log(
    //   filterLandslideSubtype.filter((x) => !existSubtype.includes(x._id))
    // );
    switch (this.state.dataHeader.maintype) {
      case "Flooding":
        return this.renderSelect(
          "subtype",
          "Sub DataType",
          Wheel.subType.flooding.filter((x) => !existSubtype.includes(x._id))
        );
      case "Landslide":
        return this.renderSelect(
          "subtype",
          "Sub DataType",
          Wheel.subType.landslide.filter((x) => !existSubtype.includes(x._id))
        );
    }
  }
  renderVisualtype(fileName) {
    //console.log(fileName.includes(".png"));
    switch (true) {
      case fileName.includes(".csv"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.timeSeries
        );
      case fileName.includes(".png"):
        return this.renderSelect(
          "visualformat",
          "Visialize Format",
          Wheel.visualFormat.textureMap
        );
    }
  }

  render() {
    //console.log(this.props.originalState.fileList);
    //console.log(this.props.originalFile);
    return (
      <div>
        {this.renderInput(
          "textureFilename",
          "File Name",
          "File Name",
          "",
          true
        )}
        <form onSubmit={this.handleSubmit}>
          <h6>Border</h6>
          <div className="form-row">
            <div className="form-group col-3">
              {this.renderInput("N", "North", "North", "")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("S", "South", "South", "")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("E", "East", "East", "")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("W", "West", "West", "")}
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-3">
              {this.renderInput("date", "Date", "Text", "dd")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("month", "Month", "Text", "mm")}
            </div>
            <div className="form-group col-3">
              {this.renderInput("year", "Year", "Text", "yyyy")}
            </div>
            {this.dateValidate()}
          </div>

          {this.renderLegendSelect()}
          {this.renderLegendUpload()}
          <button
            style={{ margin: 10 }}
            disabled={this.fileValidate()}
            className="btn btn-primary float-right"
          >
            Change
          </button>
        </form>
      </div>
    );
  }
}

export default EditTexturedataform;
