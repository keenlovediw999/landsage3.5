import "./App.css";
import Container from "react-bootstrap/Container";
import "bootstrap/dist/css/bootstrap.min.css";
import DrawChartFromUrl from "./components/drawchartfromurl";
//import React, { useEffect } from "react";
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from "react-router-dom";
console.log("Graph Application")

export default function App() {

  return (
    <Router>
      <div>
        <Container fluid>
          <Switch>
            <Route exact path="/">
              <Home
              />
            </Route>
            <Route
              exact
              path="/:dataProperty/:filename"
              children={
                <DrawGraphByFileName
                />
              }
            />
            <Route
              exact
              path="/test/:targetPort/:dataProperty/:filename"
              children={
                <DrawGraphByFileNameTest
                />
              }
            />
          </Switch>
        </Container>
      </div>
    </Router>
  );
}

function Home(props) {
  return (
    <div>
      <h1>Graph Visualizer</h1>
      <p>
        You should visit this page by "url/dataProperty/filename with extension
      </p>
      
    </div>
  );
}

function DrawGraphByFileName(props) {
  // The connection's port is preset in the environment
  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.
  //console.log("DrawGraphByFileNameFromURL");
  let { dataProperty, filename, targetPort } = useParams();
  //console.log(`check target port : ${targetPort}`);
  if (targetPort === undefined) {
    // console.log(process.env.REACT_APP_DATAPORT);
    targetPort = process.env.REACT_APP_DATAPORT || 5000;
  }
  //console.log(`check new target port : ${targetPort}`);

  return (
    <div>
      <DrawChartFromUrl
        isFillFrame={false}
        targetFilename={filename}
        targetProperty={dataProperty}
        targetPort={targetPort}
      />
    </div>
  );
}

function DrawGraphByFileNameTest(props) {
  // Mainly to create a url-oriented connection based on given url's part as a port
  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.
  //console.log("DrawGraphByFileNameFromURLCustom");
  let { targetPort, dataProperty, filename } = useParams();

  return (
    <div>
      <DrawChartFromUrl
        isFillFrame={false}
        targetFilename={filename}
        targetProperty={dataProperty}
        targetPort={targetPort}
      />
    </div>
  );
}
